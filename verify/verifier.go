package verify

import (
	"fmt"
	"log"
	"net/http"
	"verificacao-pdf/config"
	"verificacao-pdf/mapper"
)

func PdfSignatureVerify() {
	params := map[string]string {
		"nonce": config.SignatureNonce,
		"contentReturn": config.ContentReturn,
		"signatures[0][nonce]": config.SignatureNonce,
	}
	files := map[string]string {
		"signatures[0][content]": config.SignaturePath,
	}
	req, err := NewFormDataRequest(config.UrlPdfVerification, params, files)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", config.AccessToken))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	verificationReport(resp)
}


func verificationReport(response *http.Response){
	if response.StatusCode == 200 {
		pdfVerifyResponse, err := mapper.JsonMapper{}.DecodeToPdfVerifyResponse(response)
		if err != nil {
			log.Fatal(err)
		}
		log.Println(pdfVerifyResponse.Report())
	} else {
		str, err := mapper.JsonMapper{}.DecodeToString(response)
		if err != nil {
			log.Fatal(err)
		}
		log.Fatal(str)
	}
}
