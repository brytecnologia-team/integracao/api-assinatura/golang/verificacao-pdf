package config

const(
	AccessToken = "<ACCESS_TOKEN>"
)

const(
	UrlPdfVerification = "https://fw2.bry.com.br/api/pdf-verification-service/v1/signatures/verify"
)

const(
	SignaturePath = "./resource/signature.pdf"
	SignatureNonce = "1"
	ContentReturn = "true"
)
