package mapper

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"verificacao-pdf/model"
)

type JsonMapper struct {}

func (jsonMapper JsonMapper) DecodeToString(response *http.Response) (string, error){
	bytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}

func (jsonMapper JsonMapper) DecodeToPdfVerifyResponse(response *http.Response) (*model.PdfVerifyResponse, error) {
	bytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	var pdfVerifyResponse model.PdfVerifyResponse
	err = json.Unmarshal(bytes, &pdfVerifyResponse)
	return &pdfVerifyResponse, err
}

