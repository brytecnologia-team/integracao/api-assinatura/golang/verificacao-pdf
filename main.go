package main

import (
	"log"
	"verificacao-pdf/verify"
)

func main(){
	log.Println("============ PDF Verification Request")
	verify.PdfSignatureVerify()
}
