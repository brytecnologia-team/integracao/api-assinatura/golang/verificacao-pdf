package model

import (
	"strconv"
	"strings"
)

type PdfVerifyResponse struct {
	Nonce int
	VerificationStatus []VerificationStatus
}

type VerificationStatus struct {
	GeneralStatus string
	Nonce int
	SignatureFormat string
}

func (vs VerificationStatus) Report() string {
	sb := strings.Builder{}
	sb.WriteString("\t\tVerification Status: {\n")
	sb.WriteString("\t\t\tNonce: " + strconv.Itoa(vs.Nonce) + "\n")
	sb.WriteString("\t\t\tGeneral Status: " + vs.GeneralStatus + "\n")
	sb.WriteString("\t\t\tSignature Format: " + vs.SignatureFormat + "\n")
	sb.WriteString("\t\t}\n")
	return sb.String()
}


func (pvr PdfVerifyResponse) Report() string {
	sb := strings.Builder{}
	sb.WriteString("PDF Verification Response {\n")
	sb.WriteString("\tNonce:" + strconv.Itoa(pvr.Nonce) + "\n")
	sb.WriteString("\tVerifications: [\n")
	for _, status := range pvr.VerificationStatus {
		sb.WriteString(status.Report())
	}
	sb.WriteString("\t]\n")
	sb.WriteString("}\n")

	return sb.String()
}
